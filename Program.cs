﻿//https://docs.microsoft.com/en-us/dotnet/framework/network-programming/asynchronous-client-socket-example
using System;
using System.Net;  
using System.Net.Sockets;  
using System.Threading;  
using System.Text;  

namespace WindowsControlLinux
{
    public class StateObject {
        public Socket workSocket = null;
        public const int BufferSize = 256;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
        public String id = String.Empty;
    }
    class Program
    {
        private const int port = 11000;
        private static ManualResetEvent connectDone = new ManualResetEvent(false);
        private static ManualResetEvent sendDone = new ManualResetEvent(false);
        private static ManualResetEvent receiveDone = new ManualResetEvent(false);
        private static String response = String.Empty;
        private static void StartClient() {
            try {
                //IPHostEntry ipHostInfo = Dns.GetHostEntry("192.168.86.30");
                //IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPAddress ipAddress;
                IPAddress.TryParse("192.168.86.30", out ipAddress);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
                Socket client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);
                connectDone.WaitOne();

                //send id to server
                Send(client, "awcjack;id;");
                sendDone.WaitOne();
                sendDone.Reset();

                //Server send screen list (suppose filter with specific id)
                Receive(client);
                receiveDone.WaitOne();
                receiveDone.Reset();
                Console.WriteLine($"Response received: {response}");

                Send(client, "4391.awcjack_test2;quit;");
                sendDone.WaitOne();
                sendDone.Reset();
                
                Receive(client);
                receiveDone.WaitOne();
                receiveDone.Reset();
                Console.WriteLine($"Response received: {response}");

                Receive(client);
                receiveDone.WaitOne();
                receiveDone.Reset();
                Console.WriteLine($"Response received: {response}");


                client.Shutdown(SocketShutdown.Both);
                client.Close();
            } catch (Exception e){
                Console.WriteLine(e.ToString());
            }
        }
        private static void ConnectCallback(IAsyncResult ar){
            try{
                Socket client = (Socket) ar.AsyncState;
                client.EndConnect(ar);
                Console.WriteLine($"Socket connected to {client.RemoteEndPoint.ToString()}");
                connectDone.Set();
            } catch (Exception e){
                Console.WriteLine(e.ToString());
            }
        }
        private static void Send(Socket client, String data){
            byte[] byteData = Encoding.UTF8.GetBytes(data);
            client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), client);
        }
        private static void SendCallback(IAsyncResult ar){
            try {
                Socket client = (Socket) ar.AsyncState;
                int byteSent = client.EndSend(ar);
                Console.WriteLine($"Send {byteSent} bytes to server.");
                sendDone.Set();
            } catch (Exception e){
                Console.WriteLine(e.ToString());
            }
        }
        private static void Receive(Socket client){
            try{
                StateObject state = new StateObject();
                state.workSocket = client;
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize,0, new AsyncCallback(ReceiveCallback), state);
            } catch (Exception e){
                Console.WriteLine(e.ToString());
            }
        }
        private static void ReceiveCallback(IAsyncResult ar){
            String content = String.Empty;
            try{
                StateObject state = (StateObject) ar.AsyncState;
                Socket client = state.workSocket;
                int bytesRead = client.EndReceive(ar);
                if(bytesRead>0){
                    state.sb.Append(Encoding.UTF8.GetString(state.buffer,0,bytesRead));
                    content = state.sb.ToString();
                    if(content.IndexOf("screenList;") > -1){
                        response = content;
                        receiveDone.Set();
                    } else {
client.BeginReceive(state.buffer,0,StateObject.BufferSize,0,new AsyncCallback(ReceiveCallback),state);
                    }
                } else {
                    if(state.sb.Length>1){
                        response = state.sb.ToString();
                    }
                }
            } catch (Exception e){
                Console.WriteLine(e.ToString());
            }
        }
        static void Main(string[] args)
        {
            StartClient();
        }
    }
}
