﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;

namespace WindowsControlLinux
{
    public class StateObject
    {
        public Socket workSocket = null;
        public const int BufferSize = 256;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
        public String id = String.Empty;
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public String Clientid { get; set; }
        public String IPPort { get; set; }
        private Window authwindow;
        private String response = String.Empty;
        private ManualResetEvent connectDone = new ManualResetEvent(false);
        private ManualResetEvent sendDone = new ManualResetEvent(false);
        private ManualResetEvent receiveDone = new ManualResetEvent(false);
        private String screenList = String.Empty;
        private String[] ButtonName;
        private Socket server;
        public MainWindow()
        {
            InitializeComponent();
            authwindow = new authWindow(this);
        }
        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            if(Clientid == null)
            {
                authwindow.Show();
                authwindow.Activate();
            } 
        }
        public void ShowClientid()
        {
            UserID.Text = "ID: " + Clientid;
            String ip = IPPort.Split(":")[0];
            int port = int.Parse(IPPort.Split(":")[1]);
            Startserver(ip, port);
            Receive(server);//Receive server list
            receiveDone.WaitOne();
            receiveDone.Reset();
            RefreshScreenList();
            this.InvalidateVisual();
        }
        public void RefreshScreenList()
        {
            var mainContentFrame = XamlReader.Parse(screenList) as FrameworkElement;
            

            foreach(String btnName in ButtonName)
            {
                Button btn = (Button)mainContentFrame.FindName(btnName);
                btn.Click += Quit_Job;
            }
            mainContent.Content = mainContentFrame;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //authwindow = new authWindow(this);
            authwindow.Show();
            authwindow.Activate();
        }
        private void Quit_Job(object sender, RoutedEventArgs e)
        {
            Button btn = (Button) e.Source;
            String jobNameString = btn.Name + ";quit;";
            Send(server, jobNameString);
            sendDone.WaitOne();
            sendDone.Reset();

            Receive(server);
            receiveDone.WaitOne();
            receiveDone.Reset();
            RefreshScreenList();
            this.InvalidateVisual();
        }
        public void update_size(object sender, RoutedEventArgs e)
        {
            //resize --> resize button
            this.InvalidateVisual();
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }
        private String parseXaml(String[] screenList)
        {
            StringBuilder sb = new StringBuilder();
            //header
            sb.Append(@"<UserControl xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""");
            sb.Append(Environment.NewLine);
            sb.Append(@"xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml""");
            sb.Append(Environment.NewLine);
            sb.Append(@"xmlns:mc=""http://schemas.openxmlformats.org/markup-compatibility/2006""");
            sb.Append(Environment.NewLine);
            sb.Append(@"xmlns:d=""http://schemas.microsoft.com/expression/blend/2008""");
            sb.Append(Environment.NewLine);
            sb.Append(@"xmlns:local=""clr-namespace:WindowsControlLinux""");
            sb.Append(Environment.NewLine);
            sb.Append(@"mc:Ignorable=""d""");
            sb.Append(Environment.NewLine);
            sb.Append(@"d:DesignHeight=""450"" d:DesignWidth=""800"">");
            sb.Append(Environment.NewLine);
            //main part of grid & button (all use same callback function & check name)
            sb.Append("<Grid>");
            sb.Append(Environment.NewLine);
            sb.Append("<Grid.RowDefinitions>");
            sb.Append(Environment.NewLine);
            for (int i = 0; i < screenList.Length; i++)
            {
                sb.Append(@"<RowDefinition Height=""*""  MinHeight=""40"" />");
                sb.Append(Environment.NewLine);
            }
            sb.Append(" </Grid.RowDefinitions>");
            sb.Append(Environment.NewLine);
            for (int i = 0; i < screenList.Length; i++)
            {
                sb.Append("<Button x:Name=\"" + screenList[i] + "\" Grid.Row=\"" + i + "\"> " + screenList[i] + " </Button>");
                sb.Append(Environment.NewLine);
            }
            sb.Append("</Grid>");
            sb.Append(Environment.NewLine);
            sb.Append("</UserControl>");
            sb.Append(Environment.NewLine);
            return sb.ToString();
        }
        private void Startserver(String ip, int port)
        {
            try
            {
                //IPHostEntry ipHostInfo = Dns.GetHostEntry("192.168.86.30");
                //IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPAddress ipAddress;
                IPAddress.TryParse(ip, out ipAddress);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
                server = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                server.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), server);
                connectDone.WaitOne();
                //send id to server
                Send(server, $"{Clientid};id;");
                sendDone.WaitOne();
                sendDone.Reset();

                //Server send screen list (suppose filter with specific id)
                
                //receiveDone.WaitOne();
                //receiveDone.Reset();
                //System.Diagnostics.Debug.WriteLine($"Response received: {response}");

                //Send(server, "4391.awcjack_test2;quit;");
                //sendDone.WaitOne();
                //sendDone.Reset();

                //Receive(server);
                //receiveDone.WaitOne();
                //receiveDone.Reset();
                //System.Diagnostics.Debug.WriteLine($"Response received: {response}");

                //Receive(server);
                //receiveDone.WaitOne();
                //receiveDone.Reset();
                //System.Diagnostics.Debug.WriteLine($"Response received: {response}");


                //server.Shutdown(SocketShutdown.Both);
                //server.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }
        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket server = (Socket)ar.AsyncState;
                server.EndConnect(ar);
                System.Diagnostics.Debug.WriteLine($"Socket connected to {server.RemoteEndPoint.ToString()}");
                connectDone.Set();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }
        private void Send(Socket server, String data)
        {
            byte[] byteData = Encoding.UTF8.GetBytes(data);
            server.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), server);
        }
        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket server = (Socket)ar.AsyncState;
                int byteSent = server.EndSend(ar);
                System.Diagnostics.Debug.WriteLine($"Send {byteSent} bytes to server.");
                sendDone.Set();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }
        private void Receive(Socket server)
        {
            try
            {
                StateObject state = new StateObject();
                state.workSocket = server;
                server.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }
        private void ReceiveCallback(IAsyncResult ar)
        {
            String content = String.Empty;
            try
            {
                StateObject state = (StateObject)ar.AsyncState;
                Socket server = state.workSocket;
                int bytesRead = server.EndReceive(ar);
                if (bytesRead > 0)
                {
                    state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));
                    content = state.sb.ToString();
                    if (content.IndexOf("screenList;") > -1)
                    {
                        string[] fullscreenListArray = content.Split(
                            new[] { "\r\n", "\r", "\n" },
                            StringSplitOptions.None
                        );
                        //String[] fullscreenListArray = content.Split("\n");
                        String[] screenListArray = new String[fullscreenListArray.Length - 2];
                        String[] realScreenListArray = new String[screenListArray.Length];
                        foreach(String screenString in fullscreenListArray)
                        {
                            System.Diagnostics.Debug.WriteLine(screenString);
                        }
                        Array.Copy(fullscreenListArray, screenListArray, fullscreenListArray.Length - 2); //exclude screenList;
                        for(int i = 0; i < screenListArray.Length; i++)
                        {
                            if(screenListArray[i] != String.Empty)
                            {
                                String screenName = screenListArray[i].Split('\t')[1];
                                String realscreenName = screenName.Split('.')[1]; //cannot contant .
                                //String screenName = screenString.Split("\t")[2];
                                realScreenListArray[i] = realscreenName;
                            }
                        }
                        ButtonName = realScreenListArray;
                        screenList = parseXaml(@realScreenListArray);
                        response = content;
                        receiveDone.Set();
                    }
                    else
                    {
                        server.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
                    }
                }
                else
                {
                    if (state.sb.Length > 1)
                    {
                        response = state.sb.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }
    }
}
