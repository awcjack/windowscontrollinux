﻿using System;
using System.Windows;

namespace WindowsControlLinux
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class authWindow : Window
    {
        public String id = String.Empty;
        private MainWindow mainwindow; 
        public authWindow(MainWindow mainwindow)
        {
            this.mainwindow = mainwindow;
            InitializeComponent();
            if (mainwindow.Clientid != null)
            {
                idbox.Text = mainwindow.Clientid;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e) 
        {
            mainwindow.Clientid = idbox.Text;
            mainwindow.IPPort = ipbox.Text;
            mainwindow.ShowClientid();
            //this.Close();
            this.Hide();
        }
    }
}
